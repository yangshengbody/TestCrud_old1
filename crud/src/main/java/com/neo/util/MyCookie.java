package com.neo.util;

import com.jfinal.kit.HashKit;

import java.util.ResourceBundle;


public class MyCookie {
	
	public static String getCookie(){
		ResourceBundle resourceBundle = ResourceBundle.getBundle("config-dev");
		return HashKit.md5(resourceBundle.getString("cookie"));
	}
	
	public static String getCookieKey(){
		ResourceBundle resourceBundle = ResourceBundle.getBundle("config-dev");
		return HashKit.md5(resourceBundle.getString("cookieKey"));
	}
	
}
