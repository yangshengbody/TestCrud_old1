package com.neo.util;

import com.jfinal.kit.HashKit;

import java.util.ResourceBundle;


public class MySession {
	public static String getSession(){
		ResourceBundle resourceBundle = ResourceBundle.getBundle("config-dev");
		return HashKit.md5(resourceBundle.getString("session"));
	}
	
	public static String getSessionKey(){
		ResourceBundle resourceBundle = ResourceBundle.getBundle("config-dev");
		return HashKit.md5(resourceBundle.getString("sessionKey"));
	}
	
	public static String getFromSession(){
		ResourceBundle resourceBundle = ResourceBundle.getBundle("config-dev");
		return HashKit.md5(resourceBundle.getString("fromSession"));
	}
	
	public static String getFromSessionKey(){
		ResourceBundle resourceBundle = ResourceBundle.getBundle("config-dev");
		return HashKit.md5(resourceBundle.getString("fromSessionKey"));
	}
	
}
