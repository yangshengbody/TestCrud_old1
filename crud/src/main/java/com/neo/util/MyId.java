package com.neo.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


public class MyId {
	public static String getMyId(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss-");
		Date date = new Date();
		// 以  时间戳 + uuid 的方式设置Id
		String id = dateFormat.format(date) + UUID.randomUUID().toString();
		return id;
	}
}
