package com.neo.controller;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.neo.model.SysUser;
import com.neo.util.MyCookie;

import java.util.List;

public class loginModel extends Controller {
    @Inject
    SysUser sysUser;

    public void index(){
        //默认访问登录页
        renderFreeMarker("login.html");
    }
    //登录页跳转到注册页
    public void goRegister(){
        renderFreeMarker("register.html");
    }

    //注册成功跳转到登陆页面
    public void loginSuccess(){
        renderFreeMarker("login.html");
    }
//注册
    public  void  register(){
        String userName = getPara("userName");
        SysUser all = SysUser.sysUser.findFirst("select * from sys_user where userName='"+userName+"'");
        System.out.println("all="+all);
        if (all==null){
            SysUser sysItem = getModel(SysUser.class,"user");
            sysItem.save();
            System.out.println(" 注册成功");
            renderText(" 注册成功");
            redirect("/halo/loginSuccess");
        }else {
            renderText("用户名已存在");
        }
    }


    //0主页
    public void homepage(){
        renderFreeMarker("manage.html");
    }
    //登陆
    public  void login(){
        String userName = getPara("userName");
        String passWord = getPara("passWord");

        if(userName!=null){
            SysUser sysUser = SysUser.sysUser.findFirst("select * from sys_user where userName='" + userName + "'");
            System.out.println(sysUser);
            if(passWord.equals(sysUser.get("passWord"))){
                setCookie(MyCookie.getCookieKey(), MyCookie.getCookie(), 600);
                setSessionAttr("userName", userName);
                redirect("/findAll");
            }else{
                setAttr("msg" , "密码错误");
                renderFreeMarker("login.html");
            }
        }else{
            setAttr("msg" , "账号不存在！！");
            renderFreeMarker("login.html");
        }
    }

}
