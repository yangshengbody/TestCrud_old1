package com.neo.controller;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.neo.model.SysItem;

import java.util.ArrayList;
import java.util.List;

public class itemController extends Controller {
    @Inject
    SysItem  sysItem;
    public void index(){
        List<SysItem> list = SysItem.sysItem.find("select * from sys_item where flag=1");
        set("list",list);
        renderFreeMarker("manage.html");
    }

    public void delecItem(){
        String id = getPara("id");
        SysItem  find = SysItem.sysItem.findById(id);
        find.set("flag",2);
        find.update();
        System.out.println("删除成功");
       redirect("/findAll");
    }
    public void update(){
        String id = getPara("id");
        //根据 this.编辑 传过来的id值 进行查询 并回显数据
        System.out.println("我选择的ID是："+id);

        SysItem list = SysItem.sysItem.findFirst("select * from sys_item where id="+id);


        //为了方便后面的值 每个都取出
        setAttr("id", list.get("id"));
        setAttr("itemName", list.get("itemName"));
        setAttr("itemMoney", list.get("itemMoney"));
        setAttr("itemRemark", list.get("itemRemark"));
        setAttr("itemProgr", list.get("itemProgr"));
        setAttr("itemPayAttention", list.get("itemPayAttention"));
        setAttr("flag", list.get("flag"));
        renderFreeMarker("update.html");
    }
    public void updateItem(){
        SysItem sysItem = getModel(SysItem.class,"items");
        System.out.println(sysItem);
        sysItem.update();
        System.out.println(" 更新成功");
        redirect("/findAll");
    }

    public void add1(){

        renderFreeMarker("add.html");
    }
    public void add(){
        SysItem sysItem = getModel(SysItem.class,"item");
        sysItem.set("flag",1);
        sysItem.save();
        System.out.println(" 添加成功");
        redirect("/findAll");
    }
}
