package com.neo.config;

import com.jfinal.config.*;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;
import com.neo.controller.itemController;
import com.neo.controller.loginModel;
import com.neo.model.SysItem;
import com.neo.model.SysUser;

public class itemconfig extends JFinalConfig {
    private static Prop p;
    @Override
    public void configConstant(Constants constants) {

    }

    @Override
    public void configRoute(Routes me) {
        me.add("/findAll", itemController.class,"/static");
        me.add("/halo", loginModel.class,"/static");

    }

    @Override
    public void configEngine(Engine engine) {

    }
    //从配置文件中拿到 链接信息
    static void loadConfig() {
        p =  PropKit.use("jdbc.properties");

    }
    //从配置文件中拿到 数据库信息
    public static DruidPlugin getDruidPlugin() {
        loadConfig();
        return new DruidPlugin(p.get("jdbcUrl"), p.get("user"), p.get("password"));
    }
    @Override
    public void configPlugin(Plugins me) {
        DruidPlugin dbPlugin = getDruidPlugin();

        ActiveRecordPlugin arp=new ActiveRecordPlugin(dbPlugin);

        arp.setShowSql(p.getBoolean("devMode"));
        arp.setDialect(new MysqlDialect());

        dbPlugin.setDriverClass("com.mysql.jdbc.Driver");
        //把数据库中的表进行映射
        arp.addMapping("sys_item", SysItem.class);
        arp.addMapping("sys_user", SysUser.class);

        //添加到插件列表中
        me.add(dbPlugin);
        me.add(arp);

    }

    @Override
    public void configInterceptor(Interceptors interceptors) {

    }

    @Override
    public void configHandler(Handlers handlers) {

    }

    public static void main(String[] args) {
        UndertowServer.create(itemconfig.class, "undertow.properties").start();
    }
}
